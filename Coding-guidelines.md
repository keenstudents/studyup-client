# Views

### Basic structure

```html
<template>
	<!-- HTML goes here --> 
</template>

<script>
// Imports go here
import imported_com from 'path';
import shorter from 'anywhere';

export default {
	// i.e. register, login, settings
	name: 'COMPONENT/PAGE NAME', 
	// imported components
	components: {	
		'this-can-be-used-in-dom' : imported_com,
		shorter
	},
	// vue component data -> can be accessed in html aswell
	data: vm => {
		return {
			test_object: {
				username: "",
				email: "",
			},

			// if vm is specified, we can access the root and parents aswell
			rules: vm.$root.form_rules,
		}
	},
	// vue methods -> can be accessed in html aswell
	methods: {
		// Just for this component
		localFunction(bla) {
			this.bla = bla; 
		},
		// Form submit! 
		sendThisThing(){
			// Soft form validation (Tier I): Copy & paste!
			if (!this.$refs.form.validate()) return; 

			// Find your desired route in the /routes folder and handle the promise
			register(this.importantData, this.another)
			.then(response => {
				// response is an object containing a status (most likely 200), a message and finalized data
			})
			.catch(error => {
				// error is an object containing the http statuscode, a message and maybe a Node error object
			})
		}
	}
}
</script>

<style>
	/* This can be scoped so it only affects this component! */
</style>
```

# Utilities and handlers

Utilities provide additional functions and often used variables i.e. for statuscodes or regular expressions.
Handlers submit or gather data from the server through a xmlhttprequest and also deal with the traffic and errors.

# Vue and API routes

Since we are using vue router, we don't have to handle the routing on the server so it is just an API server. 
Instead, vue is handling the routing. New pages/components for the router can be registered in the router.js.

Anything requests for the server are in the /routes folder, depending on which category you work with (i.e. user specific things -> user.js).
It provides a client side API which directly communicates with the node server.


# File naming schemes

If you add a new file, make sure to seperate words with an underscore, DO NOT use camelCase.vue or so! (Only exception are components maybe)
That's ugly and inconsistent for the rest of this project

# Handling requests [IMPORTANT!]

Whenever a user clicks on a button to submit a form, the component calls a function from one of the /routes. They send a request to the server using the sendRequest
function in the request_handler. That uses the Fetch API to then return a promise. This promise is being looped back to the vue component.

(c) Copyright by Justin Falkenstein