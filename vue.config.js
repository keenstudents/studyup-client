module.exports = {
  pluginOptions: {
    cordovaPath: 'src-cordova'
  },
  devServer: {
    proxy: {
      "/": {
        target: "http://localhost:3000"
      }
    }
  },
  lintOnSave: false,
  crossorigin: 'anonymous',
  publicPath: '',
}
