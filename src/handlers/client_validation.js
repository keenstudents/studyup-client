/**
 * Validation tools
 * @summary			Some functions to validate client data
 * @file 				client_validation.js
 * @author    	Justin Falkenstein
 * @date   	  	29.06.2018
 *
 * 
 * @copyright	(c) Copyright by Justin Falkenstein
 * 
 * 
 **/


const isArray = function(a) {
	return (!!a) && (a.constructor === Array)
}

const validateArray = function(array) {
	if (!array || array.length === 0) return false

	for (let i = 0; i < array.length; i++) {
		if (isArray(array) && !validateArray(array[i])) return false
		if (isObject(array) && !validateObject(array[i])) return false
		if (array[i].length === 0 || array[i] === '') return false
	}

	return true
}


const isObject = function(a) {
	return typeof a === 'object'
}

const validateObject = function(object) {
	if (object === null || object === undefined || Object.keys(object).length === 0) return false

	for (const key in object) {
		if (object[key] === null || object[key] === undefined) return false
		if (object.hasOwnProperty(key)) {
			if (isArray(object[key]) && !validateArray(object[key])) return false
			if (isObject(object[key]) && !validateObject(object[key])) return false
			if (object[key].length === 0 || object[key] === '') return false
		}
	}

	return true
}

/**
 * Handles input from the request body
 * and checks basic validity such as types
 *
 * @note Can be extended with more functionalities
 * 			 Maybe return the invalid fields at some point
 */
export const isClientDataValid = function(...input) {
	if (input === null || input === undefined) return false
	if (isArray(input)) return validateArray(input)
	else if (isObject(input)) return validateObject(input) 
	else if (input.length === 0 || input === '') return false
		
	return true

}

/**
 * Handles incoming data
 * Used to send client responds as consistent objects
 * 
 * @note Can be extended with more functionalities
 */
export class Response {
	constructor(status = 200, message = "", data = null) {
		this.status = status
		this.message = message
		this.data = data
	}
}

/**
 * Handles incoming errors and checks their validity
 * Constructs a uniform Response object for the client
 * 
 * @note Can be extended with more functionalities
 */
export const catchError = (err, code = 500) => {
	
	if (!err) return new Response(code, 'An exception occurred but was not declared', null)
	else if (
		!(err instanceof Error) &&
		!(err instanceof EvalError) &&
		!(err instanceof RangeError) &&
		!(err instanceof ReferenceError) &&
		!(err instanceof SyntaxError) &&
		!(err instanceof TypeError) &&
		!(err instanceof URIError)
	) return new Response(code, "An exception occurred but was not of a valid Error type: ", err)

	// Error is valid, only send back the message
	return new Response(code, "Error: "+err.message, null)
}
