/**
 * Request handler
 * @summary			Handles all client request with fetch API
 * @file 				request_hander.js
 * @author    	Justin Falkenstein
 * @date   	  	18.06.2018
 *
 * 
 * @copyright	(c) Copyright by Justin Falkenstein
 * 
 * 
 **/


import { isClientDataValid, catchError } from '@/handlers/client_validation'
import CryptoJS from 'crypto-js'
import store from '@/store'

/**
 * The core function to send requests to the server
 * Uses Fetch API 
 * 
 * @param {String} 	 uri The request uri
 * @param {String} 	 method Request method
 * @param {Any} 	 	 body Client data. Can be anything
 * @param {Bool} 	 	 encrypted Whether the client data should be encrypted
 */
export const sendRequest = (uri = '/', method = 'GET', data = undefined, encrypted = false) => new Promise((resolve, reject) => {

	// Fetch options
	const DEV = process.env.NODE_ENV === 'development'  // !Enabling this deactivates CORS and switches to the offline server!
	const origin = (DEV ? uri : `http://173.249.59.82:3000${uri}`)
	const mode = (DEV ? 'same-origin' : 'cors')
	const credentials = (DEV ? 'same-origin' : 'include')
	const valid_methods = ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'PATCH']
	const body = data ? JSON.stringify((encrypted ? { enc: CryptoJS.AES.encrypt(JSON.stringify(data), 'SECRET').toString()} : data)) : undefined
	const cache = 'no-cache'
	const headers = {'Content-type': 'application/json'}
	
	const options = {
		method,
		mode,
		headers,
		credentials,
		cache,
		body
	}

	// Request and form validation
	if (valid_methods.indexOf(method) === -1) return reject(catchError(new Error("Request method invalid"), 400))
	if (method != 'GET' && !isClientDataValid(body)) return reject(catchError(new Error(`Cannot send request to URI ${uri} Client data is invalid`), 400))
	if (
		(!store.getters.user_details || store.getters.user_details === {} ) && 
		(uri !== '/user/login') && 
		(uri !== '/user/register') &&
		(uri !== '/user/logout')
		) return reject(catchError(new Error("You aren't logged in"), 403))
	
	
	// Send request using Fetch API
	fetch(origin, options)
		.then(fetch => fetch.json())
		.then(response => {
			if (response.status > 200) return reject(response)
			return resolve(response)
		})
		.catch(function(err) {
			return reject(catchError(err))
		}), function(err) {
			return reject(catchError(err))
		}
})