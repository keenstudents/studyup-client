/**
 * Social functions
 * @summary			Collection of functions to handle requests for the social route
 * @file 				social.js
 * @author    	Justin Falkenstein
 * @date   	  	18.06.2018
 *
 * 
 * @copyright	(c) Copyright by Justin Falkenstein
 * 
 * 
 **/

import { sendRequest } from "@/handlers/request_handler.js"

/**
 * Gets a paginated list of events based on filters
 * with search location and radius plus showOnlyOpenEvents boolean value
 *
 * @param {Array} categories 
 * @param {Boolean} show_open_only 
 * @param {Array} location
 * @param {Number} radius
 * @param {Number} pagination
 *
 */
export const getFilteredEvents = (categories, show_open_only, location, radius, page) => new Promise((resolve, reject) => {
	sendRequest(`/events?categories=${JSON.stringify(categories)}&openonly=${show_open_only}&loc=${JSON.stringify(location)}&lto=${radius}&page=${page}`, 'GET')
	.then(response => resolve(response))
	.catch(error => reject(error))
})

/**
 * Gets the details of a given event if allowed
 *
 * @param {ObjectId} event_id
 */
export const getEvent = (event_id) => new Promise((resolve, reject) => {
  sendRequest(`/social/event?id=${event_id}`, 'GET')
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Adds a new event
 *
 * @param {String} title The event title
 * @param {String} description The event description
 * @param {Date} date The date when the event starts
 * @param {Date} expires The date when the event ends
 * @param {Array} category The category for this event
 * @param {Array} age_range Contains 0 = min and 1 = max age
 * @param {Array} location location Location -> 0 = long 1 = lat
 * @param {String} readable_address Readable address for cosmetical reason
 * @param {Boolean} private_switch If everyone can join
 * @param {Boolean} hidden_switch If everyone can see the event
 * @param {Number} limitation Limit of members
 */
export const addEvent = (
	title,
	description,
	date,
	expires,
	category,
	age_range,
	location,
	readable_address,
	private_switch,
	hidden_switch,
	limitation
	) => new Promise((resolve, reject) => {
	let body = {
		title,
		description,
		date,
		expires,
		category,
		age_range,
		location,
		readable_address,
		private_switch,
		hidden_switch,
		limitation
	}
  sendRequest(`/user/events`, 'PUT', body, true)
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Updates details of an event
 *
 * @param {ObjectId} event_id The event id
 * @param {String} title The event title
 * @param {String} description The event description
 * @param {Date} date The date when the event starts
 * @param {Date} expires The date when the event ends
 * @param {Array} category The category for this event
 * @param {Array} age_range Contains 0 = min and 1 = max age
 * @param {Array} location Location -> 0 = long 1 = lat
 * @param {String} readable_address Readable address of location for cosmetic reasons
 * @param {Boolean} private_switch If everyone can join
 * @param {Boolean} hidden_switch If everyone can see the event
 * @param {Number} limitation Limit of members
 */
export const updateEvent = (
	event_id,
	title,
	description,
	date,
	expires,
	category,
	age_range,
	location,
	readable_address,
	private_switch,
	hidden_switch,
	limitation
	) => new Promise((resolve, reject) => {
	let body = {
		event_id,
		title,
		description,
		date,
		expires,
		category,
		age_range,
		location,
		readable_address,
		private_switch,
		hidden_switch,
		limitation
	}
	
  sendRequest(`/user/events`, 'PATCH', body, true)
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Deletes an event if the user is owner
 *
 * @param {ObjectId} event_id
 */
export const deleteEvent = (event_id) => new Promise((resolve, reject) => {
  sendRequest(`/user/events`, 'DELETE', {event_id})
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Get favorite events
 *
 *
 */
export const getFavorites = () => new Promise((resolve, reject) => {
  sendRequest(`/user/events/favorites`, 'GET')
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Toggles the event in the list of favorites of the user
 *
 * @param {ObjectId} event_id
 * @param {Boolean} toggle switch favorite to true or false
 */
export const toggleFavorite = (event_id, toggle ) => new Promise((resolve, reject) => {
  sendRequest(`/user/events/favorites`, 'PATCH', {event_id, toggle}, false)
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Applies to event
 *
 * @param {ObjectId} event_id
 */
export const subscribeEvent = (event_id) => new Promise((resolve, reject) => {
  sendRequest(`/social/event/join`, 'PUT', {event_id})
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Gets all events this user joined in
 *
 */
export const getSubscribed = () => new Promise((resolve, reject) => {
  sendRequest(`/social/event/join`, 'GET')
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Leaves an event if user is not the owner
 *
 * @param {ObjectId, UserId} event_id
 */
export const unsubscribeEvent = (event_id) => new Promise((resolve, reject) => {
  sendRequest(`/social/event/join`, 'DELETE', {event_id}, true)
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Add a comment on an event page
 *
 * @param {ObjectId} event_id
 * @param {String} text
 */
export const addComment = (event_id, text) => new Promise((resolve, reject) => {
  sendRequest(`/social/event/comments`, 'PUT', {event_id, text}, true)
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Deletes a comment on an event page
 *
 * @param {ObjectId} comment_id
 */
export const deleteComment = (comment_id) => new Promise((resolve, reject) => {
  sendRequest(`/social/event/comments`, 'DELETE', {comment_id})
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Deletes a comment on an event page
 *
 * @param {ObjectId} event_id
 */
export const getComments = (event_id) => new Promise((resolve, reject) => {
  sendRequest(`/social/event/comments?id=${event_id}`, 'GET')
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Kicks a member or deletes the pending member from the list
 *
 * @param {ObjectId} event_id Id of the event
 * @param {ObjectId} user_id Id of the user to kick
 * @param {Boolean}  ban Optional. Whether to ban the user
 */
export const kickUser = (event_id, user_id, ban = false) => new Promise((resolve, reject) => {
  sendRequest(`/social/event/takeout`, 'POST', {user_id, event_id, ban})
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Accepts a member from the pending list
 *
 * @param {ObjectId} event_id Id of the event
 * @param {ObjectId} user_id Id of the user to accept
 */
export const acceptMember = (event_id, user_id) => new Promise((resolve, reject) => {
  sendRequest(`/social/event/takein`, 'POST', {user_id, event_id})
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Bans a user
 *
 * @param {ObjectId} event_id Id of the event
 * @param {ObjectId} user_id Id of the user to unban
 */
export const banUser = (event_id, user_id) => new Promise((resolve, reject) => {
  sendRequest(`/social/event/ban`, 'PUT', {user_id, event_id})
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Unbans a user
 *
 * @param {ObjectId} event_id Id of the event
 * @param {ObjectId} user_id Id of the user to unban
 */
export const unbanUser = (event_id, user_id) => new Promise((resolve, reject) => {
  sendRequest(`/social/event/ban`, 'DELETE', {user_id, event_id})
  .then(response => resolve(response))
  .catch(error => reject(error))
})