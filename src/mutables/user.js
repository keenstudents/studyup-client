/**
 * User functions
 * @summary		  Collection of user related functions
 * @file        users.js
 * @author    	Justin Falkenstein
 * @date   	  	14.05.2018
 *
 * 
 * @copyright	(c) Copyright by Justin Falkenstein
 * 
 * 
 **/


import { sendRequest } from "@/handlers/request_handler.js"
const stub = {status: 200} // For POST requests that doesn't send data and would get catched otherwise (like logout)

/**
 * Login the user
 *
 * @param {String} username
 * @param {String} password
 */
export const login = (username, password) => new Promise((resolve, reject) => {
  sendRequest(`/user/login`, 'POST', {username, password})
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Logout the user
 */
export const logout = () => new Promise((resolve, reject) => {
  sendRequest(`/user/logout`,'POST', stub)
  .then(response => resolve(response))
  .catch(error => reject(error))
})


/**
 * Gets the settings of the current user
 */
export const getUserProfile = (user_id) => new Promise((resolve, reject) => {
  sendRequest(`/user/profile?id=${user_id}`, 'GET', undefined, true)
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Gets all events created by that user
 *
 */
export const getCreated = () => new Promise((resolve, reject) => {
  sendRequest(`/user/events`, 'GET')
  .then(response => resolve(response))
  .catch(error => reject(error))
})


/**
 * Updates the user settings
 */
export const setUserProfile = (bio, interests, avatar) => new Promise((resolve, reject) => {
  sendRequest(`/user/profile`, 'PATCH', {bio, interests, avatar}, true)
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Register a user
 *
 * @param {String} username
 * @param {String} email
 * @param {String} date_of_birth
 * @param {String} password
 * @param {Bool} 	 accepted_eula
 */
export const register = (username, email, date_of_birth, password, accepted_eula) => new Promise((resolve, reject) => {
  sendRequest(`/user/register`, 'PUT', {username, email, date_of_birth, password, accepted_eula}, true)
  .then(response => resolve(response))
  .catch(error => reject(error))
})

/**
 * Sends a comment
 *
 * @param {String} 	 text Text
 * @param {ObjectId} event_id Event
 * @param {ObjectId} user_id Poster
 *
 */
export const sendComment = (text, event_id, user_id) => new Promise((resolve, reject) => {
  sendRequest(`/social/event/comment`, 'PUT', {text, event_id, user_id}, true)
  .then(response => resolve(response))
  .catch(error => reject(error))
})


// Nah, sollten wir lassen. Realitisch betrachtet würde niemand zulassen das user einfach so feedback an den server senden 
// und der ihn da in die DB schreibt, sonst könnte man die ja fluten mit Nachrichten... und da ich echt keine Lust hab
// jetzt auch noch ne Mail API mit einzubringen, lassen wir das erstmal so xD
// ~Justin
/**
 * Sends a contact message
 *
 * @param {String} 	 text Text
 * @param {ObjectId} user_id Poster
 *
 */
/* export const sendContactMessage = (text, user_id) => new Promise((resolve, reject) => {
  sendRequest(`/social/contact`, 'POST', {text, user_id}, true)
  .then(response => resolve(response))
  .catch(error => reject(error))
})
 */