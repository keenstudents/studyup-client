import Vue from 'vue'
import Router from 'vue-router'
import Index from './views/index.vue'
import store from './store'

Vue.use(Router)

// Routes for pages and components
const routes = [
	{
		path: '/',
		name: 'home',
		component: Index,
		display_name: "StudyUp"
	},
// user related views
	{
		path: '/user/login',
		name: 'login',
		component: () => import('@/views/login.vue'),
		display_name: 'Login'
	},
	{
		path: '/user/logout',
		name: 'logout',
		component: () => import('@/views/logout.vue'),
		display_name: 'Logout'
	},
	{
		path: '/user/register',
		name: 'register',
		component: () => import('@/views/register.vue'),
		display_name: 'Register'
	},
	{ 
		// User profile aka setting for own profile
		path: '/user/profile/:id', 
		name: 'profile',
		component: () => import('@/views/profile.vue'),
		props: true,
		display_name: 'Profile'
	},
	{
		// User's created events
		path: '/user/events/created',
		name: 'user_events_created',
		component: () => import('@/views/created.vue'),
		display_name: 'Created'
	},
	{
		// User's subscribed events
		path: '/user/events/subscribed',
		name: 'user_events_subscribed',
		component: () => import('@/views/subscribed.vue'),
		display_name: 'Subscribed'
	},
	{
		// User's favorite events
		path: '/user/events/favorites',
		name: 'user_events_favorites',
		component: () => import('@/views/favorites.vue'),
		display_name: 'Favorites'
	},
	{
		path: '/social/about',
		name: 'about',
		component: () => import('@/views/about.vue'),
		display_name: 'About'
	},
	{ 
		// Single Event
		path: '/social/event/:id',
		name: 'event_details',
		component: () => import('@/views/details.vue'),
		props: true,
		display_name: 'Details'
	},
]

const router = new Router({
  routes
})

router.beforeEach((to, from, next) => {

	// Set view name per route for the navbar
	let display_name = ''
	routes.forEach(route => {
		if (route.name === to.name) display_name = route.display_name
	})
	store.commit('set_display_name', display_name)

	// Check if login needed
	if (Object.keys(store.getters.user_details).length === 0 && to.path !== '/user/login') next({ name: 'login', replace: true })
	if (Object.keys(store.getters.user_details).length !== 0 && to.path === '/user/register') next({ name: 'home', replace: true })
	next()
})

export default router