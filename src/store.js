import Vue from "vue"
import Vuex from "vuex"
import { stat } from "fs"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
		current_view: "",
    user_details: {},
    default_avatar: require("@/assets/logo.png"),
    // categories for displaying the right icons in views
    categories: [
      {
        name: "Freetime",
        description: "bla",
        icon: "umbrella-beach"
      },
      {
        name: "Sport",
        description: "bla",
        icon: "volleyball-ball"
      },
      {
        name: "Music",
        description: "bla",
        icon: "guitar"
      },
      {
        name: "Drinking",
        description: "bla",
        icon: "glass-cheers"
      },
      {
        name: "Gaming",
        description: "bla",
        icon: "desktop"
      },
      {
        name: "Nerdstuff",
        description: "bla",
        icon: "glasses"
      },
      {
        name: "Other",
        description: "bla",
        icon: "ellipsis-h"
      }
    ],
		toast: ""
	},

  mutations: {
    user_login(state, details) {
      state.user_details = details
    },
    user_logout(state) {
      state.user_details = {}
    },
    user_details(state, details) {
      state.user_details = details
    },
    remove_subscription(state, event_id) {
      let index = state.user_details.subscriptions.indexOf(event_id)
      state.user_details.subscriptions.splice(index, 1)
    },
    add_subscription(state, event_id) {
      state.user_details.subscriptions.push(event_id)
    },
    add_favorite(state, event_id) {
      state.user_details.favorites.push(event_id)
    },
    remove_favorite(state, event_id) {
      let index = state.user_details.favorites.indexOf(event_id)
      state.user_details.favorites.splice(index, 1)
    },
    update_profile(state, updated) {
        state.user_details.bio = updated.bio
        state.user_details.interests = updated.interests
        state.user_details.avatar = updated.avatar
		},
		eat_toast(state, toast) {
			state.toast = toast
		},
		set_display_name(state, name) {
			state.current_view = name
		},
		update_filters(state, filters) {
			state.filters = filters
		}
  },

  actions: {},
  
  getters: {
    user_details: state => {
      return state.user_details
    },
    default_avatar: state => {
      return state.default_avatar
    },
    default_aboutme: state => {
      return `Hi, I'm ${state.user_details.username}. This is my aboutme`
    },
    categories: state => {
      return state.categories
    },
    category_icon: state => cat => {
      let x = null
      state.categories.filter(obj => {
        if (obj.name === cat) x = obj.icon
      })
      return x
    },
    category_names: state => () => {
      let arr = []
      state.categories.filter(obj => {
        arr.push(obj.name)
      })
      return arr
		},
		filters: state => () => {
			return state.filters
		},
		logged_in: state => () => {
			return Object.keys(state.user_details).length > 0
		}
  }
})
