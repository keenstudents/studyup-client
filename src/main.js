import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import './assets/style.css'
import './plugins/vuetify'

// Imports for FontAwesomeIcons
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFilter, faPenAlt, faLock, faGlasses, faCarSide, faGuitar, faDesktop,
	faEllipsisH, faVolleyballBall, faGlassCheers, faUmbrellaBeach, faHeart, faListUl,
	faCog, faSignOutAlt, faHome, faArrowLeft, faHandPointRight, faMapMarkedAlt, faClock,
	faUsers, faUserCircle, faEdit, faCommentAlt, faTimes, faExternalLinkAlt, faQuestionCircle, faCalendarAlt, faEraser, faUserMinus, faTrashAlt, faLevelUpAlt, faCheck, faUsersCog, faChevronRight }
	from '@fortawesome/free-solid-svg-icons'
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// Adds icons to the final Build
library.add(
	faFilter, faPenAlt, faLock, faGlasses, faCarSide, faGuitar, faDesktop, faEllipsisH,
	faVolleyballBall, faGlassCheers, faUmbrellaBeach, faHeart, farHeart, faListUl,
	faCog, faSignOutAlt, faHome, faArrowLeft, faHandPointRight, faMapMarkedAlt, faClock,
	faUsers, faUserCircle, faEdit, faCommentAlt, faTimes, faExternalLinkAlt, faQuestionCircle, faCalendarAlt, faEraser, faUserMinus,
	faTrashAlt, faLevelUpAlt, faCheck, faUsersCog, faChevronRight
)

function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

// Use the Vuetify Material Icon Font
Vue.use({
	iconfont: 'md'
})

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

// Root vue component
// Everything that's being serialized here can be accessed from every child component
new Vue({
	data: () => {
		return {
			// Functions for form validation
			form_rules: {
				required: v => !!v || 'Required',
				min: v => v.length >= 8 || 'Min 8 characters',
				max: v => v.length <= 14 || 'Max 14 characters',
				min_user: v => v.length >=3 || 'Min 3 characters',
				max_user: v => v.length <= 20 || 'Max 20 Characters',
				email_valid: v => validateEmail(v) || 'E-mail must be valid',
				max_members: v => v <= 20 || 'Max 20 event members',
				min_members: v => v >= 2 || 'Min 2 event members',
				min_description: v => v.length >= 15 || 'Min 15 characters',
				max_description: v => v.length <= 100 || 'Max 100 characters',
				min_categories: v => v.length >= 5 || 'Atleast one category',
				arrayNotEmpty: v => v.length > 0 || 'Required',
				event_name: [
					v => !!v || 'Name is required',
					v => v.length <= 30 || 'Name must be less than 30 characters',
					v => v.length >= 3 || 'Name must be more than 3 characters'
				],			
			}
		}
	},
	router,
	store,
  render: h => h(App)
}).$mount('#app')
